Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :purchasers, only: [:create] do
    member do
      get 'product', to: 'purchasers#products'
    end
  end
  resources :products, only: [:create]
  post 'purchaser-product', to: 'purchasers_products#create'
end
