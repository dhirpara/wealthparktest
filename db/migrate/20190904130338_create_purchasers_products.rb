class CreatePurchasersProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :purchasers_products do |t|
      t.references :purchaser, null: false, foreign_key: true
      t.references :product, null: false, foreign_key: true
      t.datetime :purchase_timestamp, null: false

      t.timestamps
    end
  end
end
