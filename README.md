# WealthPark Test Application


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Ruby version

  - Ruby-2.6.2

* Rails vesrion

  - Rails-6.0

### Installing

A step by step series of examples that tell you how to get a development env running

- clone this repository

```
git clone https://dhirpara@bitbucket.org/dhirpara/wealthparktest.git
```

- Move to application folder after repository clone completed

```
cd wealthparktest
```

- Inatall bundle to install the application dependencey

```
bundle install
```

## Database Configuration

*sqlite3* used as database for this application.

- Run migration to create database and tables 

```
rails db:migrate
```

- check the *db/schema.rb* after migration completed successfully

## Running Application

- Start rails server

> Make sure you are in the application folder

```
rails s
```

- Check the application on browser, open the any browser of your choice and hit the following in the browser url

> Make sure server listen on port 3000

```
localhost:3000
```

## API endpoint


### Product

* url : http://localhost:3000/products
* method : POST
* body :
```
{ "name": "Test"}
```
* sample response
```
{
  "status_code": 201,
  "message": "Product is created successfully !!",
  "result": {
    "id": 1,
    "name": "Test",
    "created_at": "2019-09-05T11:12:18.838+09:00",
    "updated_at": "2019-09-05T11:12:18.838+09:00"
  }
}
```

### Purchaser

* url : http://localhost:3000/purchasers
* method : POST
* body :
```
{ "name": "Test"}
```
* sample response
```
{
  "status_code": 201,
  "message": "Purchaser is created successfully !!",
  "result": {
    "id": 1,
    "name": "Test",
    "created_at": "2019-09-05T11:14:47.304+09:00",
    "updated_at": "2019-09-05T11:14:47.304+09:00"
  }
}
```

### Purchaser-Product

* url : http://localhost:3000/purchaser-product
* method : POST
* body :
```
{
  "product_id": 1,
  "purchaser_id": 1,
  "purchase_timestamp": 1566265701
}
```
* sample response
```
{
  "status_code": 201,
  "message": "PurchasersProduct is created successfully !!",
  "result": {
    "id": 1,
    "purchaser_id": 1,
    "product_id": 1,
    "purchase_timestamp": "2019-08-20T10:48:21.000+09:00",
    "created_at": "2019-09-05T11:16:26.584+09:00",
    "updated_at": "2019-09-05T11:16:26.584+09:00"
  }
}
```

### Purchaser Purchase history

* url : http://localhost:3000/purchasers/${purchaser_id}/product?start_date=#{start_date}&&end_date=#{end_date}
* method : GET
* body :
```
{
  "start_date": "2019-08-01",
  "end_date": "2019-08-31"
}
```
* sample response
```
{
  "status_code": 201,
  "message": "",
  "result": [
    {
      "2019-08-20": [
          {
              "product_name": "test"
          }
      ]
    }
  ]
}
```
## Author

* **Divyang Hirpara** - [GitHub profile](https://github.com/divyang9389)
