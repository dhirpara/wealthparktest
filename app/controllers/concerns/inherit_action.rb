# frozen_string_literal: true
module InheritAction
  extend ActiveSupport::Concern

  def create
    @resource ||= resource_class.new(resource_params)
    if @resource.save
      yield @resource if block_given?
      render_success(@resource, I18n.t('inherit_action.created', resource_name: resource_name.classify), :created)
    else
      yield @resource if block_given?
      render_error(@resource.errors.full_messages.join(','), :unprocessable_entity)
    end
  end

  private

  def resource_class
    resource_name.classify.constantize
  end

  def resource_params
    params.permit(permitted_attributes)
  end

  def permitted_attributes
    columns = resource_class.column_names.dup
    columns.delete_if { |column| %I[id created_at updated_at].include?(column) }
  end

  def resource_name
    controller_name.singularize
  end
end

