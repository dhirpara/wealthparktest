module ResponseHandler
  module Base
    extend ActiveSupport::Concern

    def json_response(options = {})
      render json: JsonResponse.new(options), status: status
    end
  end
end
