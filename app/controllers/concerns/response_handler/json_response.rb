module ResponseHandler
  class JsonResponse
    attr_reader :status_code, :message, :result

    def initialize(options = {})
      @status_code = options[:status_code]
      @message = options[:message] || ''
      @result = options[:result] || {}
    end

    def as_json(*)
      {
        status_code: status_code,
        message: message.strip.gsub(/\s+/, ' '),
        result: result
      }
    end
  end
end
