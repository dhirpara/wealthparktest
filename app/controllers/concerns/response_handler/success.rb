module ResponseHandler
  module Success
    extend ActiveSupport::Concern
    include Base

    def render_success(resource, message, status)
      json_response({
        status_code: Rack::Utils::SYMBOL_TO_STATUS_CODE[status],
        message: message,
        result: resource
      })
    end
  end
end
