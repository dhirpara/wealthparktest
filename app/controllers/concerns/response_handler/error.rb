module ResponseHandler
  module Error
    extend ActiveSupport::Concern
    include Base

    included do
      rescue_from Exception do |error|
        handle_exception error
      end
    end

    def render_error(message, status, resource = {})
      json_response({
        status_code: Rack::Utils::SYMBOL_TO_STATUS_CODE[status] || status,
        message: message,
        result: resource
      })
    end

    def resource_not_found(resource = :resource, message = nil)
      render_error(message || I18n.t('errors.messages.resource_not_found', resource: resource.to_s.classify), :not_found)
    end

    def internal_error
      render_error(I18n.t('errors.messages.internal_server_error'), :internal_server_error)
    end

    def invalid_argument
      render_error(I18n.t('errors.messages.invalid_argument'), :unprocessable_entity)
    end

    protected

    def handle_exception(error = nil)
      case error
      when ActiveRecord::RecordNotFound
        resource_not_found
      when ArgumentError
        invalid_argument
      end
    end
  end
end
