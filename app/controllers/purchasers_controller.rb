class PurchasersController < ApplicationController
  before_action :set_purchaser, only: [:products]
  include InheritAction

  def products
    purchase_history = @purchaser.purchase_history(params[:start_date].to_date,params[:end_date].to_date)
    render_success(purchase_history, '', :created)
  end

  private
  def set_purchaser
    @purchaser = Purchaser.find_by(id: params[:id])
    unless @purchaser
      resource_not_found(resource = :purchaser, message = nil)
    end
  end
end