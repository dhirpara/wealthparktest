class PurchasersProductsController < ApplicationController
  before_action :convert_time_stamp, only: [:create]
  include InheritAction

  private
  def convert_time_stamp
    if params[:purchase_timestamp].present?
      params["purchase_timestamp"] = Time.zone.at(params[:purchase_timestamp])
    end
  end
end