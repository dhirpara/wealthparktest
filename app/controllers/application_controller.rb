class ApplicationController < ActionController::API
  include ResponseHandler::Success
  include ResponseHandler::Error
end
