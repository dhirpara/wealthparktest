class Product < ApplicationRecord
  has_many :purchasers_products, dependent: :destroy
  has_many :purchasers, through: :purchasers_products

  validates :name, uniqueness: { message: I18n.t('product.errors.name.taken') }
end
