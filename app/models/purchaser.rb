class Purchaser < ApplicationRecord
  has_many :purchasers_products, dependent: :destroy
  has_many :products, through: :purchasers_products

  validates :name, uniqueness: { message: I18n.t('purchaser.errors.name.taken') }


  def purchase_history(start_date, end_date)
    purchase_history = self.purchasers_products.joins(:product).where(purchase_timestamp: start_date.beginning_of_day..end_date.end_of_day).select("DATE(purchase_timestamp) as purchase_timestamp, products.name").group_by &:purchase_timestamp
    purchase_history.map { |ph|
      {
        "#{ph[0].strftime('%Y-%m-%d')}": ph[1].map{ |product|
          {
            product_name: product.name
          }
        }
      }
    }
  end
end
