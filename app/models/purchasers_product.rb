class PurchasersProduct < ApplicationRecord
  belongs_to :purchaser
  belongs_to :product

  validates :purchase_timestamp, presence: { message: I18n.t('errors.purchasers_product.purchase_timestamp.presence') }
  validates :purchase_timestamp, uniqueness: { scope: [:product_id, :purchaser_id], message: I18n.t('errors.purchasers_product.purchase_timestamp.taken') }
end
